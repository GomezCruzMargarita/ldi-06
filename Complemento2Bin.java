/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Programas;

import java.util.Hashtable;
import java.util.Scanner;

/**
 *
 * @author Margarita
 */
public class Complemento2Bin {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         String binario1;
      Hashtable binarios  = new Hashtable();
        Scanner entrada = new Scanner(System.in);
	binarios.put("00000001","11111110");
        binarios.put("00000011","11111100");
        binarios.put("00000111","11111000");
        binarios.put("00001111","11110000");
        binarios.put("00011111","11100000");
        binarios.put("00111111","11000000");
        binarios.put("01111111","10000000");
        binarios.put("11111111","00000000");
        binarios.put("00000101","11111010");
        binarios.put("00001001","11110110");
        binarios.put("00010001","11101110");
        binarios.put("00100001","11011110");
        binarios.put("01000001","10111110");
        binarios.put("10000001","01111110");
        binarios.put("10101010","01010101");
      
       System.out.println("Ingrese el numero binario a obtener su complemtento dos");
       binario1=entrada.next();
       
            int num1 = Integer.parseInt(binario1, 2);
            int num2 = Integer.parseInt((String) binarios.get(binario1), 2);
            int res = num1 + num2;
            String resultado = Integer.toString(res, 2);
            System.out.println("El complento a dos de " +binario1+" es: "+(String) binarios.get(binario1));
            System.out.println("El resultado de la suma de representacion de complemento a dos es: " +resultado); 
    }
    
}
